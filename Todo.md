### Core
*[x] Orbitali helper injection top side of the bootstrap
*[x] View Cache System
*[x] Laravel Translate System Extention (Working with DB) (Edited helper functions trans trans_choice __, added $default value)
*[x] Migration Publish System
*[x] Localization Capturing
*[ ] Url Managment System
*[x] Url Caching System by User 
*[ ] Meta & Title Manager
*[ ] Module System
*[x] User Authentication
*[x] Permissions System (Bouncer)
-[x] Node Module
-[x] Group Module
*[ ] File Manager
-[ ] File Module
-[x] Text Module
-[x] Input Module

### Bugs
*[ ] Cache request response fix (for login post)
*[ ] First install user_table check if exist check column
*[ ] First install first user give all permission

###Next
*[ ] Slider Module
*[ ] FormTrait for controller ( capture to form from posted request )
*[ ] Website alias or redirect type

###Test
*[ ] app/Http/helpers.php is loading ?
